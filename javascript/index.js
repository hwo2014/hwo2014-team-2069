var net = require("net");
var fs = require('fs');
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var track = (process.argv[6] != undefined) ? process.argv[6] : "keimola";

var corners = [];
console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "joinRace",
    data: {
      botId: {
      name: botName,
      key: botKey
    },
      trackName: track,
      //password: "pass666",
      carCount: 1
  }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());
var speed = 0.645;
var pieces = [];
var swap = 0;
var lane = 'Left';
var lanes = [];
var right = [];
var left = [];
var switches = [];
var notSwitching = true;
var iter = 0;
var sentl = false;
var sentr = false;
var maxSpeed = 0.8;
var turnSpeed = 0.6;
var pos = 0;
var crashCount = 0;
jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    var currpiece = pieces[data.data[0].piecePosition.pieceIndex];
    pos = data.data[0].piecePosition.pieceIndex;
      /*if(currpiece.radius != undefined){
      send({
        msgType: "switchLane",
        data: 'Right',
        gameTick: data.gameTick
      });
    }*/
  //  console.log(currpiece);
  //console.log(currpiece.radius);
  if(!notSwitching){
 //   console.log('test');
    if(data.data[0].piecePosition.pieceIndex == switches[iter]-1){
   //   console.log('test');
      if(left[iter] == 1){
        console.log('right');
	sentl=true;
      send({
        msgType: "switchLane",
        data: "Right",
	gameTick: data.gameTick
      });
	iter++;
      }else if(right[iter] == 1){
        console.log('left');
	sentr = true;
        send({
          msgType: "switchLane",
          data: "Left",
          gameTick: data.gameTick
        });
	iter++;
      }
    }
    notSwitching = true;
  }
sentr=false;
sentl=false;
for(var p=0; p<5;p++){
  if(pieces[data.data[0].piecePosition.pieceIndex+p] != undefined){
if(pieces[data.data[0].piecePosition.pieceIndex+p].angle != undefined 
  && corners[data.data[0].piecePosition.pieceIndex+p] != undefined){
  var id = null;
  for(var l=0; l<corners.length;l++){
    if(corners[l][0] == data.data[0].piecePosition.pieceIndex+p){
      speed=corners[l][1];
    }
  }
}else{
  speed=0.9
  if(crashCount>2){
    speed -= (crashCount-1)/10;
    crashCount = 0;
  }
}
}}

for(var a=0;a<corners.length;a++){
  if(pieces[pos-p] == pieces[corners[a][0]] && corners[a][1] < 1){
    corners[a][1] += 0.05;
    console.log(corners[a][1])
    console.log('a');
  }
}
  if(notSwitching){
   if(sentr == false || sentl == false){
    send({
      msgType: "throttle",
      data: speed,
      gameTick: data.gameTick
    });
    notSwitching = false;
  }}
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      iter=0;
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended\n final speed: '+speed+'\n final time: '+data.data.results[0].result.millis);
      fs.writeFileSync(track,JSON.stringify(corners));
    } else if (data.msgType === 'crash' && data.data.name == botName){
      crashCount++;
      for(var r = 0; r<5; r++){
        for(var a = 0; a<corners.length;a++){
          if(pieces[pos-r] == pieces[corners[a][0]]){
            corners[a][1] -= 0.1;
            console.log('reduced speed: '+corners[a]);
          }
        }
      }
      console.log('Crashed! See replay.');
    } else if (data.msgType === 'spawn'){
      console.log('Respawned, starting race..');
    } else if (data.msgType === 'turboAvailable'){
      console.log('rawr, turbo!');
      send({
        msgType: "turbo",
        data: "rawr"
      });
    }

     else if (data.msgType === 'lapFinished' && data.){
      console.log('Finished lap!\n Time: '+data.data.lapTime.millis);
      iter=0;
    } else if (data.msgType === 'gameInit'){
      pieces = data.data.race.track.pieces;
      console.log(pieces);
      lanes = data.data.race.track.lanes;
      console.log(lanes);

        for(var i = 0; i<pieces.length; i++){
      if(pieces[i].switch != undefined && pieces[i].angle == undefined){
        switches.push(i);
        if(pieces[i+1].angle != undefined){
          if(fs.readFileSync(track).length < 1){
            corners.push([i,1]);
            console.log(corners.length);
          }else{
            //corners = fs.readFileSync(track).toArray();
            corners = JSON.parse(fs.readFileSync(track,{flag: 'r'}));
          }
          if(pieces[i+1].angle > 0){
            left.push(1);
            right.push(0);
          }else 
          if(pieces[i+1].angle < 0){
            left.push(0);
            right.push(1);
          }
        }
      }
  }
      console.log('r:\n'+right+'\nl:\n'+left);
      console.log(switches);
      console.log(corners);
    }else{
      console.log(data);
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
