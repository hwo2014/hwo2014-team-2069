var net = require("net");
var fs = require('fs');
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var track = (process.argv[6] != undefined) ? process.argv[6] : "keimola";

var corners = [];
console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "joinRace",
    data: {
      botId: {
      name: botName,
      key: botKey
    },
      trackName: track,
      //password: "pass666",
      carCount: 1
  }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());
var speed = 1;
var pieces = [];
var pieceArr = [];
var swap = 0;
var lane = 'Left';
var lanes = [];
var right = [];
var left = [];
var switches = [];
var notSwitching = true;
var iter = 0;
var sentl = false;
var sentr = false;
var maxSpeed = 0.8;
var turnSpeed = 0.6;
var pos = 0;
var crashCount = 0;
jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    var currpiece = pieces[data.data[0].piecePosition.pieceIndex];
    pos = data.data[0].piecePosition.pieceIndex;
      /*if(currpiece.radius != undefined){
      send({
        msgType: "switchLane",
        data: 'Right',
        gameTick: data.gameTick
      });
    }*/
  //  console.log(currpiece);
  //console.log(currpiece.radius);
  if(!notSwitching){
 //   console.log('test');
    if(data.data[0].piecePosition.pieceIndex == switches[iter]-1){
   //   console.log('test');
      if(left[iter] == 1){
        console.log('right');
  sentl=true;
      send({
        msgType: "switchLane",
        data: "Right",
  gameTick: data.gameTick
      });
  iter++;
      }else if(right[iter] == 1){
        console.log('left');
  sentr = true;
        send({
          msgType: "switchLane",
          data: "Left",
          gameTick: data.gameTick
        });
  iter++;
      }
    }
    notSwitching = true;
  }
sentr=false;
sentl=false;

  if(notSwitching){
   if(sentr == false || sentl == false){
    send({
      msgType: "throttle",
      data: speed,
      gameTick: data.gameTick
    });
    notSwitching = false;
  }}
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      iter=0;
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended\n final speed: '+speed+'\n final time: '+data.data.results[0].result.millis);

    } else if (data.msgType === 'crash' && data.data.name == botName){
      console.log('Crashed! See replay.');
    } else if (data.msgType === 'spawn'){
      console.log('Respawned, starting race..');
    } else if (data.msgType === 'turboAvailable'){
      
    }

     else if (data.msgType === 'lapFinished'){
      console.log('Finished lap!\n Time: '+data.data.lapTime.millis);
      iter=0;
    } else if (data.msgType === 'gameInit'){
      pieces = data.data.race.track.pieces;
      console.log(pieces);
      lanes = data.data.race.track.lanes;
      console.log(lanes);

        for(var i = 0; i<pieces.length; i++){
          pieceArr.push([]);
          for(var j = 0; j<Math.round(((pieces[i]["length"] != undefined) ? pieces[i]["length"]/10 : pieces[i]["radius"]/10 )); j++){
            pieceArr[i].push(1);
        }
      if(pieces[i].switch != undefined && pieces[i].angle == undefined){
        switches.push(i);
        if(pieces[i+1].angle != undefined){
          
          if(pieces[i+1].angle > 0){
            left.push(1);
            right.push(0);
          }else 
          if(pieces[i+1].angle < 0){
            left.push(0);
            right.push(1);
          }
        }
      }
  }
      console.log('r:\n'+right+'\nl:\n'+left);
      console.log(switches);
      console.log(pieceArr);
    }else{
      console.log(data);
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
